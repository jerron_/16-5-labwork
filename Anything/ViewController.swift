//
//  ViewController.swift
//  Anything
//
//  Created by GDD Student on 16/5/16.
//  Copyright © 2016 OMTA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func buttonWasTapped(sender: AnyObject) {
        let alert:UIAlertController = UIAlertController(title: "Button Tapped!", message: "A button was tapped on the screen.", preferredStyle: .Alert)
        let action:UIAlertAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Default){(a: UIAlertAction) -> Void in
            print("item selected")
        }
        let action1:UIAlertAction = UIAlertAction(title: "Item 1", style: UIAlertActionStyle.Default){(a: UIAlertAction) -> Void in
            print("item 1 selected")
        }
        alert.addAction(action)
        alert.addAction(action1)

         self.presentViewController(alert, animated: true)
            {
        () -> Void in print("alert presented")
        }
    }
   
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func SliderDidChange(sender: UISlider) {
        let percent:Float = sender.value / sender.maximumValue
        progressBar.progress = percent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

